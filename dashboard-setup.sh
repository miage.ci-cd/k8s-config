#!/usr/bin/sh

echo "Deploying Kubernetes Dashboard..."
kubectl apply -f kubernetes-dashboard.yaml

echo -e "\nSetting up admin account permissions..."
kubectl apply -f admin-user.yaml

echo -e "\n\nServices deployed succesfully."
echo -e "\nDashboard URL: $HOSTNAME:30443"
echo -e "Bearer Token:"

secret=$(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')
kubectl -n kube-system describe secret $secret \
	| grep token: \
	| awk -F "[[:space:]]+" '{ print $2 }'

echo -e "\n"
